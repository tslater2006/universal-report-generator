/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package universalreportgenerator;

import java.sql.DriverManager;

/**
 *
 * @author tslater
 */
public class UniversalReportGenerator {

    static {
        // Register JDBC Drivers
        try
        {
            // My SQL
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            
            // MSSQL
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
            
            // Oracle
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        }catch (Exception e){
            System.out.println("Failed to load all JDBC Drivers!");
            e.printStackTrace(System.out);
            System.exit(-1);
        }
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
    }
    
    
}
